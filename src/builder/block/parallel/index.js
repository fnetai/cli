async function hits({ node }) {
    return false;
}

async function init({ node, initNode }) {
}

async function resolve({ node, resolveTypeCommon, resolveNextBlock, transformExpression, transformValue }) {
}

module.exports = {
    hits,
    init,
    resolve
}